# Tools Vulnerability Workshop - 2020 🧰

Welcome! 🥳

This file describes the contents of this package, as well as some links that will help you get started. The goal of this workshop is to help you understand how people use static analysis tools for security, the security guarantees by these tools, how soundness issues can compromise those guarantees, and how we can find soundness issues in those tools. Besides, we also hope to make you familiar with the [Responsible Disclosure](https://en.wikipedia.org/wiki/Responsible_disclosure) approach. When you find a bug/vulnerability in a tool, you report it to us only and no-one else. Of course, you are not allowed to discuss your findings with any of the participating members during the workshop.

Furthermore, you are required to provide details that will help us understand what exactly you found, why you think it is a vulnerability, and possible impacts.

## Package Description

In this package, you will be finding logs from static analysis based crypto API misuse detection tools. The organization is as follows:

```shell
|-- apps
|   |-- android
|   `-- java
|-- masc_logs
|   |-- android
|   `-- java
`-- tool_logs
    |-- cryptoguard
    |-- crysl
    `-- findsecbugs
```

### apps

Apps contain the mutated codes of both java and Android applications. 

### MASC Logs

`MASC` stands for **M**utation based **A**nalysis of **S**tatic **C**rypto-misuse detectors. When given the source code of a base application in Java language format, the tool explores the code and mutates it according to pre-defined rules. The base applications can be either android or java applications. The log types are of the following types:

- reachability mutation logs
- selective mutation logs

Reachability mutation logs are about introducing mutations at every location possible to understand whether these locations are "reachable" by a particular tool. In the relevant log files, the lines start with `leak`.

Selective mutation logs are about introducing mutations "selectively" based on existing crypto API usage in the base app. Furthermore, it creates separate code bases per mutation, whereas, for reachability, it makes only one mutated code base that contains all mutations! In relevant logs, the lines start with `Mutant`. Note that selective mutation may introduce multiple misuse instances.

Both types of logs should give you a sufficient level of detail about the location of the mutation within the source code of the application.

## Tool Logs

The tool logs are distributed by the tools, of course.

- number suffix (such as `qpid-mutant1.log`) are analysis of selective mutation
- `_original` indicates that the log is of an original, un-mutated base app
- others are analysis of reachability based mutations

You may notice that analysis results do not include all the numbers from the mutation log. For example, we mutated `qpid` selectively more than 30 times, but here we only provide analysis results of 6 of them to keep things simpler. 😄

Please note that the number mapping are a bit different for `qpid`.

- mutation id 32 refers to analysis report 1
- mutation id 33 refers to analysis report 2
- mutation id 35 refers to analysis report 3
- mutation id 8 refers to analysis report 4
- mutation id 2 refers to analysis report 9
- mutation id 23 refers to analysis report 17

Few different things can happen when we analyze mutated applications using a tool:

- For misuse we introduced through mutation, a tool can either report or not report it.
- A tool can also report/not report misuses already existing in the code base, i.e., before we perform mutation.

In the case a tool does not report a misuse, preexisting, or introduced, it may or may not be a flaw! Why? The next section describes that!

## Tools

These tools are either from academia or industry and come with adequate documentation related to what they _(claim to)_ detect! For ease of access, I have linked tools with their documentation here:

- CryptoGuard - [paper](https://arxiv.org/pdf/1806.06881.pdf), [github](https://github.com/CryptoGuardOSS/cryptoguard)
- CrySL - [paper](https://github.com/CROSSINGTUD/CryptoAnalysis), [github](https://github.com/CROSSINGTUD/CryptoAnalysis)
- FindBugs/SpotBugs with Find-Security-Bugs Extension - [SpotBugs](https://spotbugs.github.io/), [Find-Sec-Bugs](https://find-sec-bugs.github.io/)

Going through the documentation is **important** because if a tool claims that it only looks for misuse 'A' 'B' and 'C' and you report a flaw related to misuse 'D' - it is not a flaw.

## Reporting Issues

So you found an issue in a tool? Great! To make a complete report, you **must** include the following:

- Description of Issue
- Locations related to Issue, such as related logs, location in source code and relevant code snippets
- Reasoning about why you consider this as an issue
- Impact in terms of compromising the security of an application

You will gain points based on the completeness of your report.

(this readme is specifically for the students of W&M, will be made private once the workshop is over.)
